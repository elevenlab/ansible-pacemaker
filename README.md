Ansible Role: Pacemaker
===================
Install and configure [Pacemaker](https://github.com/ClusterLabs/pacemaker) cluster.
It will install pacemaker on Debian machines and it will configure resources to be handled by the cluster.
It will also deploy a modified version of pgsql `resource-agent` allowing to dynamically add new memebers to the cluster without the need of restarting the cluster. (see `files/pgsql`)

Due to Debian 8 old version of pacemaker, this role will provide the `jessie-backports` source list to the machine and a pcs client ported from Debian 9

----------

Role variables
-------------

Available variables are listed below, along with default values (see `defaults/main.yml`):

	pacemaker_configure: false
If configure is set to `true` the role will also configure resorce. **It will destroy** any previous cluster and recreate it. Use `pacemaker_configure` **only ** for the first installation.

	pacemaker_ansible_group: ''
The Ansible inventory group containing cluster peers. The role will walk the group members and configure them as members of a cluster.

	pacemaker_cluster_name: hacluster
	pacemaker_user: hacluster
	pacemaker_password: hacluster
The name of the cluster, the user managing it and the plaintext password for the mentioned user. It will be hashed with per-host salt to maintain idempotency.

	pacemaker_token_timeout: 3000
	pacemaker_token_retransmits_before_loss_const: 10
	pacemaker_clear_node_high_bit: 'yes'
	pacemaker_crypto_cipher: aes256
	pacemaker_crypto_hash: sha256
	pacemaker_transport: udpu
	pacemaker_log_to_file: 'yes'
	pacemaker_logfile: /var/log/corosync/corosync.log
	pacemaker_log_to_syslog: 'yes'
	pacemaker_log_timestamp: 'on'
	pacemaker_auth_key: ''
These are some default value for *pacemaker* and *corosync* configuration.  Valid values for `crypto_cipher` are `none` (no encryption), `aes256`, `aes192`, `aes128` and  `3des`. Enabling `crypto_cipher`, requires also enabling of `crypto_hash`.
`pacemaker_transport` allow to specify to use unicast or multicast `udp` protocol.

The auth key `pacemaker_auth_key` (default empty) is the local path where pick the auth key for the cluster. The auth key will be placed in `/etc/corosync/authkey` as expected by the cluster.

	pacemaker_properties: {}
A key/value set of global properties to apply to the cluster once up and running.
The keys of this dict with underscores correspond to pacemaker properties with hyphens. 

Be sure to **quote** cluster properties! By default, YAML parser will guess variable types, so the string `"false"` will be converted to Boolean `False` and then to string `"False"` mismatching what you expect.
Pacemaker properties are case-sensitive, e.g. `stonith-enabled=False` will be accepted, but STONITH will still be on because it would expect `stonith-enabled=false`.
Correct example:

	pacemaker_properties:
		stonith_enabled: "false"

---

	pacemaker_resource_default: {}
A key/value set of default value that apply to each resource the not override it. The keys of this dict with underscores correspond to pacemaker resource default with hyphens.

	pacemaker_resources: []
An array of resource definitions. Each definition is a dict of two mandatory members, `id` (resource name) and `type` (`standard:provider:type string`, see output of `pcs resource providers`).
They can also have optional members:
	
    pacemaker_resources:
        - id: <id>
          type: <standard:provider:type>
          options:
              key: value
          op:
            - action: <[start | stop | monitor]>
              options:
                  key: value

          disabled: false
          wait: <n/a>
Resource options is a key/vaule set adding specific resource options.

The keys `disabled` and `wait` might be present: the former starts the resource disabled, the last force the cluster to wait for resource to start for the number of seconds defined.

The op list, is a list of elements that define the operation on the resource. Each op has an `action` and his relate `options`. A common set of options for op are: 

	timeout: <string> # e.g: 60s
    interval: <string> # e.g: 4s
    on_fail: <string> # an action e.g: restart

Additionally, there might be **mutually exclusive** members: Boolean `clone`,  dict ~~masterslave~~ * or dict `group` with their respective options (see `man 8 pcs`). 

If group is specified, all the resources with the same `group.name` will be grouped together.
Moreover you can set `group.before` **or** `group.after` to specify the position of the added resources relatively to some resource already existing in the group.

	group:
		name: GroupName
		options:
			key: value
	# or
	clone: true
	# or
	masterslave:
	    name: MasterName
		options:
			key:value
***** master has a **bug** in the pcs client when used together with create resource to workaround use instead `pacemaker_resources` without master and than:

	pacemaker_resource_master: []
This provide a list of master resource definition. Each definition has a `name` a reference to the resource `resource` and a key/value `options`. Frequent options are: 
			
			master_max: <int>
            master_node_max: <int>
            clone_max: <int>
            clone_node_max: <int>
            notify: <"true" | "false">

---

	pacemaker_constraint_colocation: []
A list of colocation constraints for the resources. Each item can have all the following members (see `man 8 pcs` for further details):

-	`source_resource`: the id of resource
-	`source_type: [master | slave]`: if defined means `source_resource` is the master or the slave resource associated, otherwise `source_resource` represent the resource itself
-	`target_resource`: the id  `source_resource` is bound to
-	`target_type`: the `target_resource` resource type
-	`score`: the score of the constraint (positive means try to stick together while negative means avoid to stick together)
-	`options`: a key/value set which contains additional options

it will translate to:
	
	pcs colocation add [<source_type>] <source_resource> with [<target_type>] <target_resource> [score] [options]
> *collocation add (man 8 pcs)*:
> 
>Request `<source_resource> `to run on the same node where pacemaker has determined `<target_resource>` should run.
>Positive values of `<score>` mean the resources should be run on the same node, negative values mean the resources should not be run on the same node.  Specifying `INFINITY` (or `-INFINITY`) for the score forces `<source_resource>` to run (or not run) with `<target_resource>` resource (score defaults to `INFINITY`).
>A role can be master or slave.

----
	pacemaker_constraint_order: []
A list of order constraint. It will apply to action of 2 resources. 
It has the members: 

	first_action: < promote | demote | start | stop | notify > # see man 8 pcs
    first_resource: <resource-id>
    second_action: < promote | demote | start | stop | notify >
    second_resource: <resource-id>
    options:
        key: value
	
The `first_action` action of the `first_resource` resource will set to happend before the `second_action` of the `second_resource`. Frequent options are `score: <int | +-INFINITY>`, `symmetrical: <"true" | "false">`

	pacemaker_smtp_alert: {}
This variable allow to set an alert for the events that happen inside the cluster. It will bind to the alert-agent located in `files/alert_smtp.sh` which is deployed to the machine in the path: `/usr/share/pacemaker/alerts/alert_smtp.sh`.

The alert definition has these members:

	id: <id>
	options:
		key: value
    recipients:
	    - id:  <id>
	      value: <email>
Yuo must specify the id of the alert as well as the dedicated options of the agent. for the `alert_smtp.sh`provided, the `options` are:

	email_sender:
    email_client: swaks
    auth_server:
    auth_user:
    auth_password:
The role will also install `swaks` as mail sender, allowing to specify additional parameters like auth server, user and password. 
The `recipients` is a list of the email recipients that will receive the alert.
   
Dependencies
-------------------
None.

Example Playbook
--------------------------

    - name: install and configure pacemaker
      hosts: databases
      vars:
        pacemaker_ansible_group: databases
        pacemaker_properties:
            no_quorum_policy: stop
            stonith_enabled: "false"
        pacemaker_resource_default:
            resource_stickiness: 100000
            migration_threshold: 1
        pacemaker_resources:
            - id: vip-master
              type: "ocf:heartbeat:IPaddr2"
              options:
                ip: 10.0.0.100
                cidr_netmask: 32
                iflabel: m
              op:
                - action: start
                  options:
                    timeout: 60s
                    interval: 0s
                    on_fail: restart
                - action: monitor
                  options:
                    timeout: 60s
                    interval: 10s
                    on_fail: restart
                - action: stop
                  options:
                    timeout: 60s
                    interval: 0s
                    on_fail: block
              group:
                name: master-group 
            - id: vip-rep
              type: "ocf:heartbeat:IPaddr2"
              options:
                ip: 10.0.0.110
                cidr_netmask: 32
                iflabel: r
              op:
                - action: start
                  options:
                    timeout: 60s
                    interval: 0s
                    on_fail:  restart
                - action: monitor
                  options:
                    timeout: 60s
                    interval: 10s
                    on_fail: restart
                - action: stop
                  options:
                    timeout: 60s
                    interval: 0s
                    on_fail: stop
              group:
                name: master-group
                after: vip-master
            - id: postgresql-id
              type: "ocf:heartbeat:pgsql"
              options:
                pgctl: /usr/lib/postgresql/9.4/bin/pg_ctl
                psql: /usr/bin/psql
                pgdata: /var/lib/postgresql/9.4/main
                config: /etc/postgresql/9.4/main/postgresql.conf
                logfile: /var/log/postgresql/postgresql-9.4-main.log
                pgdba: postgres
                pgport: 5432
                socketdir: /var/run/postgresql
                rep_mode: sync
                repuser: replication-user
                primary_conninfo_opt: "sslmode=prefer sslcompression=1 keepalives_idle=60 keepalives_interval=5 keepalives_count=5"
                master_ip: 10.0.0.110
                restart_on_promote: "false"
                replication_slot_name: replication_slot
                check_wal_receiver: "true"
                tmpdir: /var/lib/postgresql/tmp/
              op:
                - action: start
                  options:
                    timeout: 60s
                    interval: 0s
                    on_fail: restart
                - action: monitor
                  options:
                    timeout: 60s
                    interval: 4s
                    on_fail: restart
                - action: monitor
                  options:
                    timeout: 60s
                    interval: 3s
                    on_fail: restart
                    role: Master
                - action: promote
                  options:
                    timeout: 60s
                    interval: 0s
                    on_fail: restart
                - action: demote
                  options:
                    timeout: 60s
                    interval: 0s
                    on_fail: stop
                - action: stop
                  options:
                    timeout: 60s
                    interval: 0s
                    on_fail: block
                - action: notify
                  options:
                    timeout: 60s
                    interval: 0s
        pacemaker_resource_master:
            - name: master-pgsql
              resource: postgresql-id
              options:
                master_max: 1
                master_node_max: 1
                clone_max: "{{ groups['databases'] | length }}"
                clone_node_max: 1
                notify: "true"
        pacemaker_constraint_colocation:
            - source_resource: master-group
              target_resource: master-pgsql
              target_role: master
              score: "INFINITY"
        pacemaker_constraint_order:
            - first_action: promote
              first_resource: master-pgsql
              second_action: start
              second_resource: master-group
              options:
                score: "INFINITY"
                symmetrical: "false"
            - first_action: demote
              first_resource: master-pgsql
              second_action: stop
              second_resource: master-group
              options:
                score: 0
                symmetrical: "false"
        pacemaker_smtp_alert:
              id: smtp_alert
              options:
                email_sender: hacluster@cluster.com
                email_client: swaks
                auth_server: xxxxxx
                auth_user: xxxxxx
                auth_password: xxxxxxx
              recipients: alert@example.com
      roles:
        - { role: pacemaker, pacemaker_configure: true }
